const axios = require('axios');
const querystring = require('querystring');

class ApplicationAuthenticationProvider {
  /**
   * Initialize the authentication provider with the AAD credentials
   * @param {string} tenantId Microsoft Azure Active Directory Application tenant id
   * @param {string} clientId Microsoft Azure Active Directory Application client id
   * @param {string} clientSecret Microsoft Azure Active Directory Application client secret
   */
  constructor(tenantId, clientId, clientSecret) {
    // Microsoft Azure Active Directory Application tenant id
    this.tenantId = tenantId;
    // Microsoft Azure Active Directory Application client id
    this.clientId = clientId;
    // Microsoft Azure Active Directory Application client secret
    this.clientSecret = clientSecret;

    // Module params
    // Microsoft Graph base url
    this.baseUrl = 'https://login.microsoftonline.com/';
    // Microsoft Graph access token
    this.accessToken = null;
    // Type of the grant
    this.grantType = 'client_credentials';
    // Scope of the authentication
    this.scope = 'https://graph.microsoft.com/.default';

    // The token expiry time
    this.expireTime = null;
    this.setExpireTime = 0;
  }

  /**
   * Create a new axios instance
   * @returns {AxiosInstance}
   */
  get axiosInstance() {
    return axios.create({
      baseURL: this.baseUrl,
      headers: {
        client: 'IndexAuthenticationProvider',
      },
    });
  }

  /**
   * Set the expiry time of the authentication
   */
  set setExpireTime(secondsToAdd) {
    this.expireTime = Math.round((new Date()).getTime() / 1000) + secondsToAdd;
  }

  /**
   * Get the token endpoint for the tenant
   * @returns {string} token endpoint
   */
  get tokenEndpoint() {
    return `${this.tenantId}/oauth2/v2.0/token`;
  }

  /**
   * Generate token url query string
   * @returns {string} token url query string
   */
  get tokenUrlQueryString() {
    return querystring.stringify({
      client_id: this.clientId,
      client_secret: this.clientSecret,
      scope: this.scope,
      grant_type: this.grantType,
    });
  }

  /**
   * Request a new token
   * @returns {Promise}
   */
  async requestNewToken() {
    try {
      // Get the token data from the token endpoint
      const { data } = await this.axiosInstance.post(
        this.tokenEndpoint,
        this.tokenUrlQueryString,
      );

      console.log('[MSGRAPH][APPLICATION] Got new access token');

      // Set the expireTime and the accessToken
      this.setExpireTime = data.expires_in;
      this.accessToken = data.access_token;
    } catch (error) {
      return error;
    }

    return null;
  }

  /**
   * This method is called every time we try to access the api
   *
   * If the token is equal or past it's expiry time, request a new token
   * Else return the token we already have
   * @returns {Promise<string>}
   */
  async getAccessToken() {
    // Get the current time
    const currentTime = Math.round((new Date()).getTime() / 1000);

    // Check if the authentication has expired already, or if there is no token yet
    if (this.accessToken === null || this.expireTime <= currentTime) {
      console.log(`[MSGRAPH][APPLICATION] Application token expired for ${this.clientId} acquire new token`);
      // Request a new access token
      try {
        await this.requestNewToken();
      } catch (e) {
        console.error(`[MSGRAPH][APPLICATION] Could not get new access token for application ${this.clientId}`);
        console.error(e.response.data);
        return e;
      }
    }

    // Return the access token
    return this.accessToken;
  }
}

module.exports = ApplicationAuthenticationProvider;
