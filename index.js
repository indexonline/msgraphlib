const { Client } = require('@microsoft/microsoft-graph-client');
const ApplicationAuthenticationProvider = require('./ApplicationAuthenticationProvider');
const UserAuthenticationProvider = require('./UserAuthenticationProvider');

const {
  MS_GRAPH_TENANT_ID,
  MS_GRAPH_CLIENT_ID,
  MS_GRAPH_CLIENT_SECRET,
  MS_GRAPH_REDIRECT_ENDPOINT,
  MS_GRAPH_CLIENT_SCOPES,
  MS_GRAPH_DEFAULT_VERSION,
} = process.env;

module.exports.UserAuthenticationProvider = UserAuthenticationProvider;
module.exports.ApplicationAuthenticationProvider = ApplicationAuthenticationProvider;

/**
 * Return the configures MsGraph Client
 * @returns {object} MsGraph Client
 */
module.exports.applicationClient = Client.initWithMiddleware({
  defaultVersion: MS_GRAPH_DEFAULT_VERSION,
  authProvider: new ApplicationAuthenticationProvider(
    MS_GRAPH_TENANT_ID,
    MS_GRAPH_CLIENT_ID,
    MS_GRAPH_CLIENT_SECRET,
  ),
});

/**
 * Create a redirect uri for a hostname
 * @param {string} hostname hostname for the redirect uri
 * @returns {string} redirect uri
 */
module.exports.msGraphRedirectUri = (hostname) => {
  const prefix = hostname === 'localhost' ? 'http' : 'https';

  const baseURL = `${prefix}://${hostname}`;

  const redirectUri = new URL(MS_GRAPH_REDIRECT_ENDPOINT, baseURL);

  return redirectUri.toString();
};

/**
 * Return the configures MsGraph Client
 * @returns {object} MsGraph Client
 */
module.exports.userClient = (
  accessToken,
  refreshToken,
  expiresAfter,
  hostname,
  userId,
  metaUpdateHandler,
) => Client.initWithMiddleware({
  defaultVersion: MS_GRAPH_DEFAULT_VERSION,
  authProvider: new UserAuthenticationProvider(
    MS_GRAPH_TENANT_ID,
    MS_GRAPH_CLIENT_ID,
    MS_GRAPH_CLIENT_SECRET,
    this.msGraphRedirectUri(hostname),
    accessToken,
    refreshToken,
    expiresAfter,
    userId,
    metaUpdateHandler,
  ),
});

/**
 * Return the scopes, separated by space
 * @returns {string} Scopes, separated by space
 */
module.exports.scopes = MS_GRAPH_CLIENT_SCOPES.split(',').join(' ');
