const axios = require('axios');
const querystring = require('querystring');
const { scopes } = require('./index.js');

const errorTypes = {
  50173: 'revokedAccessToken',
  50011: 'replyUrlNotMatchUrlsConfiguredForApplication',
};

class UserAuthenticationProvider {
  /**
   * Initialize the authentication provider with the AAD credentials
   * @param {string} tenantId Microsoft Azure Active Directory Application tenant id
   * @param {string} clientId Microsoft Azure Active Directory Application client id
   * @param {string} clientSecret Microsoft Azure Active Directory Application client secret
   * @param {string} redirectUri Microsoft Azure Active Directory Redirect URI
   * @param {string} accessToken User JWT Token
   * @param {string} refreshToken User Refresh
   * @param {number} expiresAfter Seconds since epoch after which token wil be expired expired
   * @param {async function} metaUpdateHandler Function to update meta handler
   */
  constructor(
    tenantId,
    clientId,
    clientSecret,
    redirectUri,
    accessToken,
    refreshToken,
    expiresAfter,
    userId,
    metaUpdateHandler,
  ) {
    // Microsoft Azure Active Directory Application tenant id
    this.tenantId = tenantId;
    // Microsoft Azure Active Directory Application client id
    this.clientId = clientId;
    // Microsoft Azure Active Directory Application client secret
    this.clientSecret = clientSecret;
    // Microsoft Azure Active Directory Redirect URI
    this.redirectUri = redirectUri;

    // Module params
    // Microsoft Graph base url
    this.baseUrl = 'https://login.microsoftonline.com/';
    // Microsoft Graph access token
    this.accessToken = accessToken;
    // Microsoft Graph refresh token
    this.refreshToken = refreshToken;
    // The token expiry time
    this.expireTime = expiresAfter;
    // The token expiry time
    this.userId = userId;

    // Grant type
    this.grantType = 'refresh_token';

    // Meta update handler
    this.metaUpdateHandler = metaUpdateHandler;
  }

  /**
   * Create a new axios instance
   * @returns {AxiosInstance}
   */
  get axiosInstance() {
    return axios.create({
      baseURL: this.baseUrl,
      headers: {
        client: 'IndexAuthenticationProvider',
      },
    });
  }

  /**
   * Generate token url query string
   * @returns {string} token url query string
   */
  get tokenUrlQueryString() {
    return querystring.stringify({
      client_id: this.clientId,
      grant_type: this.grantType,
      scope: scopes,
      refresh_token: this.refreshToken,
      redirect_uri: this.redirectUri,
      client_secret: this.clientSecret,
    });
  }

  /**
   * Get the token endpoint for the tenant
   * @returns {string} token endpoint
   */
  get tokenRefreshEndpoint() {
    return `${this.tenantId}/oauth2/v2.0/token`;
  }

  /**
   * Set the expiry time of the authentication
   */
  set setExpireTime(secondsToAdd) {
    this.expireTime = Math.round((new Date()).getTime() / 1000) + secondsToAdd;
  }

  /**
   * Request a new token
   * @returns {Promise}
   */
  async requestNewToken() {
    try {
      const { data } = await this.axiosInstance({
        method: 'POST',
        url: this.tokenRefreshEndpoint,
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: this.tokenUrlQueryString,
      });

      console.log(`[MSGRAPH][USER] Got new access token for user ${this.userId}`);

      // Set the expireTime, refreshToken and the accessToken
      this.setExpireTime = data.expires_in;
      this.accessToken = data.access_token;
      this.refreshToken = data.refresh_token;

      // Update the user's token data
      await this.metaUpdateHandler(this.userId, {
        msGraph: {
          accessToken: this.accessToken,
          refreshToken: this.refreshToken,
          expiresAfter: this.expireTime,
        },
      });
    } catch (e) {
      console.log(e);
      console.error(`[MSGRAPH][USER] Could not get new access token for user ${this.userId}`);

      // Get the error codes from the response data
      const { error_codes: errorCodes } = e.response.data;

      // Map the errors to readable error type
      const errors = errorCodes.map((code) => errorTypes[code]);

      // Check if we have a known error
      if (errors.length) {
        await Promise.all(
          // For each error do something
          errors.map(async (error) => {
            // Different errors require different solutions
            switch (error) {
              // If the user access token has been revoked
              case 'revokedAccessToken':
                // Update the user's token data
                await this.metaUpdateHandler(this.userId, {
                  msGraph: {
                    accessToken: null,
                    refreshToken: null,
                    expiresAfter: null,
                  },
                });
                break;
              default:
                break;
            }
            return true;
          }),
        );
        console.log(errors, errorCodes);
      } else {
        // We have an unknown error
        console.error(e.response.data);
      }

      return e;
    }

    return null;
  }

  /**
   * This method is called every time we try to access the api
   *
   * If the token is equal or past it's expiry time, request a new token
   * Else return the token we already have
   * @returns {Promise<string>}
   */
  async getAccessToken() {
    // Get the current time
    const currentTime = Math.round((new Date()).getTime() / 1000);

    // Check if the authentication has expired already, or if there is no token yet
    if (this.accessToken === null || this.expireTime <= currentTime) {
      console.log(`[MSGRAPH][USER] Token for user ${this.userId} is expired`);
      // Request a new access token
      try {
        await this.requestNewToken();
      } catch (e) {
        return e;
      }
    }

    // Return the access token
    return this.accessToken;
  }
}

module.exports = UserAuthenticationProvider;
